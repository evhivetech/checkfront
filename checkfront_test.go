package checkfront

import (
	"testing"
)

func TestGetBooking(t *testing.T) {

	//initialization
	account := checkFrontAccount{
		AuthKey:    "",
		Identifier: "TEST",
		EndPoint:   "https://test-sandboxvrind.checkfront.com",
	}

	var c Config

	c.CheckFrontAccounts = append(c.CheckFrontAccounts, account)

	checkFrontObj := New(c)

	//testing

	response, err := checkFrontObj.GetBooking("YHMN-110619", "TEST")

	if err != nil {
		t.Error(err)
	}

	if response.Request.Records == 0 {
		t.Error("error getting response")
	}

	t.Log(response)
}

func TestSetBookingPaid(t *testing.T) {

	//initialization
	account := checkFrontAccount{
		AuthKey:    "",
		Identifier: "TEST",
		EndPoint:   "https://test-sandboxvrind.checkfront.com",
	}

	var c Config

	c.CheckFrontAccounts = append(c.CheckFrontAccounts, account)

	checkFrontObj := New(c)

	//testing

	response, err := checkFrontObj.SetBookingPaid("YHMN-110619", "TEST")

	if err != nil {
		t.Error(err)
	}

	if response.Request.Data.StatusID != "PAID" {
		t.Error("status not updated")
	}

	t.Log(response)
}
